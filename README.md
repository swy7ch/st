Swy7ch' fork of st
==================

This is my fork of suckless' terminal emulator, [st](https://st.suckless.org/). It is tweaked to match my needs and comes with just a few patches:

- [st-anysize](https://st.suckless.org/patches/anysize/)
- [st-clipboard](https://st.suckless.org/patches/clipboard/)
- [st-scrollback](https://st.suckless.org/patches/scrollback/)

Keybindings
-----------

- **Alt+u / Alt+d** .....Scrolls a whole page up/down
- **Alt+k / Alt+j** .....Scrolls a line up/down
- **Alt+r / Alt+l** .....Zoom in/out
- **Alt+c** .....Copies to xclipboard
- **Alt+v** .....Pastes from xclipboard

That's it!
----------

No need for anything else. I tweaked the cursor to a | , changed the color to the gruvbox theme, and set the font to be **FuraCode Nerd Font Mono**. Enjoy!

---

# st - simple terminal
--------------------
st is a simple terminal emulator for X which sucks less.


Requirements
------------
In order to build st you need the Xlib header files.


Installation
------------
Edit config.mk to match your local setup (st is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install st (if
necessary as root):

    make clean install


Running st
----------
If you did not install st with make clean install, you must compile
the st terminfo entry with the following command:

    tic -sx st.info

See the man page for additional details.

Credits
-------
Based on Aurélien APTEL <aurelien dot aptel at gmail dot com> bt source code.

